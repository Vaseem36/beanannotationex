package com.cts.proj.model.AnnotationEx;

import org.springframework.context.annotation.Bean;

import com.cts.project.model.Product;

public class AppInit {
	@Bean
	public Product getProduct() {
		return new Product(1001,"santoor",67.89);
	}

}
