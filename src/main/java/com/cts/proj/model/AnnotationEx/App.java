package com.cts.proj.model.AnnotationEx;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.cts.project.model.Product;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext ctx=new AnnotationConfigApplicationContext(AppInit.class);
        Product p=ctx.getBean(Product.class);
        System.out.println(p);
    }
}
